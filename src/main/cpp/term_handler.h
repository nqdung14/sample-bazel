#include "src/main/cpp/backtrace.h"


namespace xmpl {

static void DumpException() {
  static bool thrown = false;

  try {
    // try once to re-throw currently active exception
    if (!thrown) {
      thrown = true;
      throw;
    }
  } catch (const std::exception &e) {
    std::cerr << "Caught unhandled exception. what(): " << e.what() << std::endl;
  } catch (...) {
    std::cerr << "Caught unknown/unhandled exception." << std::endl;
  }

  // backtrace will be dumped in signal handler
  std::abort();
}

static void DumpOnSignal(int p) {
  std::cerr << "Caught signal " << strsignal(p) << std::endl;
  std::cerr << "Backtrace: " << std::endl;
  for (auto &msg : Backtrace(128)) {
    std::cerr << msg << std::endl;
  }

  // call the default handler
  signal(p, SIG_DFL);
  raise(p);
}

static void SetTerminateHandler() {
  signal(SIGABRT, DumpOnSignal);
  signal(SIGSEGV, DumpOnSignal);
  std::set_terminate(DumpException);
}

}
