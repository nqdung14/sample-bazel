#include <functional>
#include <iostream>
#include <signal.h>

#include <glog/logging.h>
#include <folly/futures/Future.h>
#include <folly/executors/ThreadedExecutor.h>
#include <folly/executors/IOThreadPoolExecutor.h>

#include "src/main/cpp/term_handler.h"
#include "src/main/cpp/echo_server.h"


DEFINE_int32(port, 3333, "Port");

static bool interrupted;

static folly::Promise<int> exit_promise;
static folly::Future<int> exit_future = exit_promise.getFuture();

static void HandleInterrupt(int) {
  if (!interrupted) {
    LOG(INFO) << "User interrupted. Stopping... Press Ctrl+C again to perform a hard exit.";
    interrupted = true;
  } else {
    exit_promise.setValue(1);
  }
}

int main(int argc, char *argv[]) {
  FLAGS_logtostderr = true;
  // Initialize Google's logging library.
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);

  xmpl::SetTerminateHandler();
  signal(SIGINT, HandleInterrupt);


  const auto num_threads = std::max(2, static_cast<int>(std::thread::hardware_concurrency()));
  LOG(INFO) << "Num threads: " << num_threads;
  folly::IOThreadPoolExecutor executor(num_threads);
  xmpl::socket::EchoServer server;

  auto main_event_base = executor.getEventBase();
  std::vector<folly::EventBase *> evbs;
  for (int i = 0; i < static_cast<int>(num_threads) - 1; ++i) {
    evbs.emplace_back(executor.getEventBase());
  }
  
  main_event_base->runInEventBaseThread([&server, main_event_base, &evbs]() mutable {
    server.InitializeEventBase(main_event_base, evbs);
    server.Start(FLAGS_port);
  });
  return std::move(exit_future).get();
}
